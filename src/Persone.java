import java.io.*;
import java.math.*;
import java.time.*;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class Persone {

    private ArrayList<Persona> elencoPersone;

    public Persone(){
        elencoPersone= new ArrayList<Persona>();
    }

    public Persone(ArrayList<Persona> persone){
        this();
        for (int i = 0; i < persone.size(); i++) {
            elencoPersone.add(persone.get(i));
        }
    }

    public Persone(Persone persone){
        this();
        for (int i = 0; i < persone.elencoPersone.size(); i++) {
            this.elencoPersone.add(persone.elencoPersone.get(i));
        }
    }

    public void addPersona(Persona p){
        elencoPersone.add(p);
    }

    public boolean insPersona(Persona p, int index){
        if(index>=0 && index<=elencoPersone.size()){
            elencoPersone.add(index, p);
            return true;
        }
        return false;
    }

    public boolean removePersona(int index){
        if(index>=0 && index<elencoPersone.size()){
            elencoPersone.remove(index);
            return true;
        }
        return false;
    }

    public Persona getPersona(int i){
        if(i < elencoPersone.size()&& i>=0){
            return elencoPersone.get(i);
            
        }
        return null;
    }

    public void loadCSV(String elencoDati) {
        try (Scanner s = new Scanner(new File(elencoDati)).useDelimiter(System.lineSeparator())) {
            s.nextLine();
            while (s.hasNextLine()) {
                String[] record = s.nextLine().split(";");
                addPersona(new Persona(record[0], LocalDate.parse(record[1]),BigDecimal.valueOf(Double.parseDouble(record[2])),Boolean.parseBoolean(record[3])));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public Persone trovaDebitori(){
        Persone debitori = new Persone();
        for(Persona p: this.elencoPersone){
            if((!p.getIsPagato()) && p.getDistanzaGiorni() > 0){
                debitori.elencoPersone.add(p);
            }
        }
        return debitori;
    }

    public String toString(){
        String x = "Elenco persone: ";
        for (int i = 0; i < elencoPersone.size(); i++) {
            x += elencoPersone.get(i).toString() + System.lineSeparator();
        }
        return x;
    }

    
    
}
