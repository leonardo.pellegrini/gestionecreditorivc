import java.math.*;
import java.time.*;
import java.util.*;
import java.math.BigDecimal;
import java.time.temporal.ChronoUnit;

public class Persona {
    private String nominativo;
    private LocalDate data_scadenza;
    private BigDecimal importo;
    private boolean isPagato;


    public Persona() {
    }

    public Persona(String nominativo, LocalDate data_scadenza, BigDecimal importo, boolean isPagato) {
        this.nominativo = nominativo;
        this.data_scadenza = data_scadenza;
        this.importo = importo;
        this.isPagato = isPagato;
    }

    public Persona(Persona p){
        this.nominativo = p.getNominativo();
        this.data_scadenza = p.getData_scadenza();
        this.importo = p.getImporto();
        this.isPagato = p.getIsPagato();
    }

    public String getNominativo() {
        return this.nominativo;
    }

    public void setNominativo(String nominativo) {
        this.nominativo = nominativo;
    }

    public LocalDate getData_scadenza() {
        return this.data_scadenza;
    }

    public void setData_pagamento(LocalDate data_pagamento) {
        this.data_scadenza = data_pagamento;
    }

    public BigDecimal getImporto() {
        return this.importo;
    }

    public void setImporto(BigDecimal importo) {
        this.importo = importo;
    }

    public boolean getIsPagato() {
        return this.isPagato;
    }

    public void setIsPagato(boolean isPagato) {
        this.isPagato = isPagato;
    }

    public long getDistanzaGiorni(){
        long days = ChronoUnit.DAYS.between(getData_scadenza(),LocalDate.now());
        return days;
    }

    public String toString() {
        return "{" +
            " nominativo='" + getNominativo() + "'" +
            ", data_pagamento='" + getData_scadenza() + "'" +
            ", importo='" + getImporto() + "'" +
            ", isPagato='" + getIsPagato() + "'" + 
            ", distanza giorni='" +getDistanzaGiorni()+
            "}";
    }
}

